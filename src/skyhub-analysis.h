#ifndef _SKYHUB_ANALYSIS
#define _SKYHUB_ANALYSIS

#include <gst/gst.h>

#include "skyhub-stream.h"

GstElement *skyhub_stream_analysis_bin_new(skyhub_stream_t *stream);

#endif
