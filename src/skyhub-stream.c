#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>


#include <errno.h>

#include "gstnvdsmeta.h"
//#include "gstnvstreammeta.h"
#ifndef PLATFORM_TEGRA
#include "gst-nvmessage.h"
#endif

#include "skyhub-stream.h"
#include "skyhub-analysis.h"
#include "skyhub-util.h"

#define MAX_DISPLAY_LEN 64

#define PGIE_CLASS_ID_VEHICLE 0
#define PGIE_CLASS_ID_PERSON 2

/* The muxer output resolution must be set if the input streams will be of
 * different resolution. The muxer will scale all the input frames to this
 * resolution. */
#define MUXER_OUTPUT_WIDTH 1920
#define MUXER_OUTPUT_HEIGHT 1080

/* Muxer batch formation timeout, for e.g. 40 millisec. Should ideally be set
 * based on the fastest source's framerate. */
#define MUXER_BATCH_TIMEOUT_USEC 4000000

#define TILED_OUTPUT_WIDTH 1920
#define TILED_OUTPUT_HEIGHT 1080

/* NVIDIA Decoder source pad memory feature. This feature signifies that source
 * pads having this capability will push GstBuffers containing cuda buffers. */
#define GST_CAPS_FEATURES_NVMM "memory:NVMM"

static skyhub_stream_t *_stream = NULL;

static gboolean bus_call (GstBus * bus, GstMessage * msg, gpointer data)
{
	GMainLoop *loop = (GMainLoop *) data;
	switch (GST_MESSAGE_TYPE (msg)) {
		case GST_MESSAGE_EOS:
			g_print ("End of stream\n");
			g_main_loop_quit (loop);
			break;
		case GST_MESSAGE_WARNING:
			{
				gchar *debug;
				GError *error;
				gst_message_parse_warning (msg, &error, &debug);
				g_printerr ("WARNING from element %s: %s\n",
						GST_OBJECT_NAME (msg->src), error->message);
				g_free (debug);
				g_printerr ("Warning: %s\n", error->message);
				g_error_free (error);
				break;
			}
		case GST_MESSAGE_ERROR:
			{
				gchar *debug;
				GError *error;
				gst_message_parse_error (msg, &error, &debug);
				g_printerr ("ERROR from element %s: %s\n",
						GST_OBJECT_NAME (msg->src), error->message);
				if (debug)
					g_printerr ("Error details: %s\n", debug);
				g_free (debug);
				g_error_free (error);
				g_main_loop_quit (loop);
				break;
			}
#ifndef PLATFORM_TEGRA
		case GST_MESSAGE_ELEMENT:
			{
				if (gst_nvmessage_is_stream_eos (msg)) {
					guint stream_id;
					if (gst_nvmessage_parse_stream_eos (msg, &stream_id)) {
						g_print ("Got EOS from stream %d\n", stream_id);
					}
				}
				break;
			}
#endif
		default:
			break;
	}
	return TRUE;
}


int skyhub_stream_source_add(skyhub_stream_t *stream, const char *uri) 
{
	int rc = 0;
	char *source_uri = NULL;

	if (stream == NULL || uri == NULL) {
		g_printerr("%s: Error: Invalid parameters\n", __FUNCTION__);
		rc = -EINVAL;
		goto exit;
	}

	source_uri = strdup(uri);

	if (source_uri == NULL) {
		rc = -ENOMEM;
		goto exit;
	}

	stream->sources = g_list_append(stream->sources, source_uri);

exit:
	return rc;
}

static gboolean link_streamux_to_decode(GstElement *streammux, GstElement *decode)
{
	GstPad *sinkpad, *srcpad;
	gchar pad_name_sink[16] = "sink_0";
	gchar pad_name_src[16] = "src";

	sinkpad = gst_element_get_request_pad (streammux, pad_name_sink);
	if (!sinkpad) {
		g_printerr ("Streammux request sink pad failed. Exiting.\n");
		return FALSE;
	}

	srcpad = gst_element_get_static_pad (decode, pad_name_src);

	if (!srcpad) {
		g_printerr ("link: decode request src pad failed. Exiting.\n");
		return FALSE;
	}

	GstPadLinkReturn ret = gst_pad_link (srcpad, sinkpad);
	if (ret != GST_PAD_LINK_OK) {
		g_printerr ("Failed to link decoder to stream muxer. Exiting. %d\n", ret);
		return FALSE;
	}

	gst_object_unref (sinkpad);
	gst_object_unref (srcpad);

	return TRUE;
}

static void stream_parsebin_new_pad_cb(GstElement *parsebin, GstPad *src_pad, gpointer data)
{
	GstPadLinkReturn ret = GST_PAD_LINK_OK;
	//GstElement *urisourcebin = (GstElement *) data;
	GstPad *sink_pad = NULL;
	GstElement *bin = NULL;
	GstElement *tee = NULL;

	tee = create_element("tee", "tee");

	GstElement *parent = (GstElement *)gst_object_get_parent((GstObject *) parsebin);

	gst_bin_add(GST_BIN(parent), tee);
	gst_element_sync_state_with_parent(tee);

	sink_pad = gst_element_get_static_pad(tee, "sink");

	g_assert(sink_pad != NULL);

	ret = gst_pad_link(src_pad, sink_pad);

	g_assert(ret == GST_PAD_LINK_OK);

	bin = skyhub_stream_analysis_bin_new(_stream);

	g_assert(bin != NULL);

	gst_bin_add(GST_BIN(parent), bin);

	GstPad *tee_src = gst_element_get_request_pad(tee, "src_%u");

	g_assert(tee_src != NULL);

	GstPad *analysis_sink = gst_element_get_static_pad(bin, "sink");

	g_assert(analysis_sink != NULL);

	ret = gst_pad_link(tee_src, analysis_sink);

	g_assert (ret == GST_PAD_LINK_OK);

	GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(_stream->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "skyhub-pipeline");
}

static void stream_urisourcebin_new_pad_cb (GstElement * urisourcebin, GstPad * src_pad, gpointer data)
{
	GstPad *sink_pad = NULL;
	GstPadLinkReturn ret = GST_PAD_LINK_OK;
	GstElement *parsebin = (GstElement *) data;

	sink_pad = gst_element_get_static_pad(parsebin, "sink");

	g_assert(sink_pad != NULL);

	ret = gst_pad_link(src_pad, sink_pad);

	g_assert(ret == GST_PAD_LINK_OK);
}

static void stream_create_uri_source_bin (guint index, char * uri)
{
	GstElement *urisourcebin = NULL;
	GstElement *parsebin = NULL;

	/* Source element for reading from the uri.
	 * We will use urisourcebin and let it figure out the uri handler for the
	 * stream and plug the appropriate protocol plugins. */
	urisourcebin = create_element ("urisourcebin", "urisourcebin");

	parsebin = create_element("parsebin", "parsebin");


	/* set the input uri to the source element */
	g_object_set (G_OBJECT (urisourcebin), "uri", uri, NULL);

	/* Connect to the "pad-added" signal of the urisourcebin which generates a
	 * callback once a new pad for raw data has beed created by the urisourcebin */
	g_signal_connect (G_OBJECT (urisourcebin), "pad-added",
			G_CALLBACK (stream_urisourcebin_new_pad_cb), parsebin);

	/* Connect to the "pad-added" signal of the parsebin which generates a
	 * callback when a new pad has been created by parsebin*/
	g_signal_connect (G_OBJECT (parsebin), "pad-added",
			G_CALLBACK (stream_parsebin_new_pad_cb), urisourcebin);


	gst_bin_add_many (GST_BIN (_stream->pipeline), urisourcebin, parsebin, NULL);
}

static int initialize_sources(skyhub_stream_t *stream) {
	int rc = 0;
	int n_sources = 0;

	if (stream == NULL) {
		g_printerr("%s: Error: Invalid parameters\n", __FUNCTION__);
		rc = -EINVAL;
		goto exit;
	}

	// verify the sources list has been initialized.
	if (stream->sources == NULL) {
		rc = -1;
		goto exit;
	}

	n_sources = g_list_length(stream->sources);

	// there isn't anything to be done if there isnt at least one source
	if (n_sources < 1) {
		rc = -1;
		goto exit;
	}

	/* this will iterate through all of the sources that were
	 * added with skyhub_stream_source_add and attach them
	 * to the streammux element. */
	int i = 0;
	for (GList *l = stream->sources; l != NULL; l = l->next) {
		char *source_uri = (char *) l->data;
		stream_create_uri_source_bin (i, source_uri);
		i++;
	}
exit:
	return rc;
}

// dont expose this, currently there isnt a good way to dynamically set the model
// path because the model/<name/config.txt has hardcoded paths to the weights
// and parameter files
static gboolean skyhub_stream_set_model_path(skyhub_stream_t *stream, gchar * path)
{
	if (stream == NULL || path == NULL) {
		return FALSE;
	}

	stream->model_path = g_strdup(path);

	if (stream->model_path == NULL) {
		return FALSE;
	}
	return TRUE;
}

gboolean skyhub_stream_set_model(skyhub_stream_t *stream, gchar *model)
{
	if (stream == NULL || model == NULL) {
		return FALSE;
	}

	stream->active_model = g_strdup(model);

	if (stream->active_model == NULL) {
		return FALSE;
	}
	return TRUE;
}


skyhub_stream_t * skyhub_stream_new(SkyhubStreamType stream_type, GMainLoop *loop, char *model)
{
	skyhub_stream_t *stream = NULL;
	int rc = 0;


	if (!skyhub_stream_is_valid_model(model)) {
		printf("Error: stream=%s is invalid!\n", model);
		return NULL;
	}

	stream = malloc(sizeof(*stream));

	if (stream == NULL) {
		printf("Allocating stream != NULL failed\n");
		goto exit;
	}
	memset(stream, 0, sizeof(*stream));

	/* This is exposed so we can dump the graph from any point */
	_stream = stream;

	if (!skyhub_stream_set_model(stream, model)) {
		printf("Error: skyhub_stream_set_model failed\n");
		return NULL;
	}

	stream->loop = loop;

	stream->type = stream_type;

	switch (stream_type) {
		case SKYHUB_STREAM_TYPE_FISHEYE:
			stream->max_sources = SKYHUB_FISHEYE_MAX;
			break;
		case SKYHUB_STREAM_TYPE_PTZ:
			stream->max_sources = SKYHUB_PTZ_MAX;
			break;
		default:
			g_printerr ("Failed to link source bin to stream muxer. Exiting.\n");
			rc = -1;
			goto exit;
	}

	stream->pipeline = create_pipeline("skyhub-pipeline");

exit:
	if (rc != 0) {
		free(stream);
		stream = NULL;
	}

	g_assert(stream != NULL);

	return stream;
}

int skyhub_stream_start(skyhub_stream_t *stream)
{
	int rc = 0;
	GstBus *bus = NULL;

	rc = initialize_sources(stream);

	g_assert(rc == 0);

	/* we add a message handler */
	bus = gst_pipeline_get_bus (GST_PIPELINE (stream->pipeline));

	g_assert(bus != NULL);

	stream->bus_watch_id = gst_bus_add_watch (bus, bus_call, stream->loop);
	gst_object_unref (bus);
	gst_element_set_state (stream->pipeline, GST_STATE_PLAYING);

	if (gst_element_get_state (stream->pipeline, NULL, NULL, -1) == GST_STATE_CHANGE_FAILURE) {
		g_error ("Failed to go into PLAYING state");
	}

	return rc;
}

void skyhub_stream_stop(skyhub_stream_t *stream) {
	if (stream) {
		g_print ("Returned, stopping playback\n");
		gst_element_set_state (stream->pipeline, GST_STATE_NULL);
		g_print ("Deleting pipeline\n");
		gst_object_unref (GST_OBJECT (stream->pipeline));
		g_source_remove (stream->bus_watch_id);
		g_main_loop_unref (stream->loop);
	}
}

void skyhub_stream_free(skyhub_stream_t *stream) {
	if (stream) {
		g_free(stream->active_model);
		g_free(stream->model_path);
		memset(stream, 0, sizeof(skyhub_stream_t));
		g_free(stream);
	}
}
