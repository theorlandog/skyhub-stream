#include <gst/gst.h>
#include <glib.h>

GstElement * create_element(const char *factory, const char *name)
{
	GstElement *element = NULL;


	if (factory == NULL || name == NULL) {
		g_printerr("%s: Error: Invalid name or factory\n", __FUNCTION__);
		goto exit;
	}

	element = gst_element_factory_make(factory, name);

	if (element == NULL) {
		g_printerr("%s: Failed creating element factory=%s, name=%s\n", __FUNCTION__, factory, name);
		goto exit;
	}

exit:
	g_assert(element != NULL);

	return element;
}

GstElement *create_pipeline(const char *name)
{
	GstElement *element = NULL;

	if (name == NULL) {
		g_printerr("%s: Error: Invalid name\n", __FUNCTION__);
		goto exit;
	}

	element = gst_pipeline_new(name);

	if (element == NULL) {
		g_printerr("%s: Failed creating pipeline name=%s\n", __FUNCTION__, name);
		goto exit;
	}
exit:
	g_assert(element != NULL);

	return element;
}
