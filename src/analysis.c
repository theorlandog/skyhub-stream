#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>


#include <errno.h>

#include "gstnvdsmeta.h"

#ifndef PLATFORM_TEGRA
#include "gst-nvmessage.h"
#endif

#include "skyhub-stream.h"
#include "skyhub-util.h"


#define PGIE_CLASS_ID_VEHICLE 0
#define PGIE_CLASS_ID_PERSON 2

/* The muxer output resolution must be set if the input streams will be of
 * different resolution. The muxer will scale all the input frames to this
 * resolution. */
#define MUXER_OUTPUT_WIDTH 1920
#define MUXER_OUTPUT_HEIGHT 1080

/* Muxer batch formation timeout, for e.g. 40 millisec. Should ideally be set
 * based on the fastest source's framerate. */
#define MUXER_BATCH_TIMEOUT_USEC 4000000


gchar pgie_classes_str[4][32] = {
	"Vehicle",
	"TwoWheeler",
	"Person",
	"RoadSign"
};



typedef struct _analysis_model_t {
	gchar *name;
	gchar *config;
	gchar classes[8][16];
} analysis_model_t;

static analysis_model_t g_model[1] = {
	// Primary Detector, DetectNet_v2 model with Resnet10 as backbone
	{ 
		.name = "primary", 
		.config = "/opt/skyhub/models/Primary_Detector/config.txt", 
		.classes = { 
			"Vehicle",
			"TwoWheeler",
			"Person",
			"RoadSign"
		}
	}
};

static skyhub_stream_t *_stream = NULL;

GList *skyhub_stream_list_models()
{
	GList *list = NULL;
	int n_engines = 0;

	n_engines = sizeof(g_model) / sizeof(analysis_model_t);

	for (int i = 0 ; i < n_engines; i++) {
		list = g_list_append(list, g_model[i].name);
	}

	return list;
}

gchar *skyhub_stream_get_model_config(skyhub_stream_t *stream) {
	int n_models = 0;

	if (stream == NULL) {
		return NULL;
	}

	n_models = sizeof(g_model) / sizeof(analysis_model_t);

	for (int i = 0 ; i < n_models; i++) {
		if (strcmp(g_model[i].name, stream->active_model) == 0) {
			return g_model[i].config;
		}
	}
	return NULL;

}

gboolean skyhub_stream_is_valid_model(gchar *name) {
	gboolean is_valid = FALSE;
	int n_models = 0;

	if (name == NULL) {
		return FALSE;
	}

	n_models = sizeof(g_model) / sizeof(analysis_model_t);

	for (int i = 0 ; i < n_models; i++) {
		if (strcmp(g_model[i].name, name) == 0) {
			return TRUE;
		}
	}
	return FALSE;
}
/* tiler_sink_pad_buffer_probe  will extract metadata received on OSD sink pad
 * and update params for drawing rectangle, object information etc. */
static GstPadProbeReturn tiler_src_pad_buffer_probe(
		GstPad * pad, 
		GstPadProbeInfo * info,
		gpointer u_data)
{
	GstBuffer *buf = (GstBuffer *) info->data;
	guint num_rects = 0; 
	NvDsObjectMeta *obj_meta = NULL;
	guint vehicle_count = 0;
	guint person_count = 0;
	NvDsMetaList * l_frame = NULL;
	NvDsMetaList * l_obj = NULL;

	NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);

	for (l_frame = batch_meta->frame_meta_list; l_frame != NULL;
			l_frame = l_frame->next) {
		NvDsFrameMeta *frame_meta = (NvDsFrameMeta *) (l_frame->data);
		//int offset = 0;
		for (l_obj = frame_meta->obj_meta_list; l_obj != NULL;
				l_obj = l_obj->next) {
			obj_meta = (NvDsObjectMeta *) (l_obj->data);
			if (obj_meta->class_id == PGIE_CLASS_ID_VEHICLE) {
				vehicle_count++;
				num_rects++;
			}
			if (obj_meta->class_id == PGIE_CLASS_ID_PERSON) {
				person_count++;
				num_rects++;
			}
		}

		if (vehicle_count > 0 || person_count > 0) {
			g_print ("Frame Number = %d Number of objects = %d "
					"Vehicle Count = %d Person Count = %d\n",
					frame_meta->frame_num, num_rects, vehicle_count, person_count);
		}

	}
	return GST_PAD_PROBE_OK;
}

static gboolean link_streamux_to_decode(
		GstElement *streammux, 
		GstElement *decode)
{
	GstPad *sinkpad, *srcpad;
	gchar pad_name_sink[16] = "sink_0";
	gchar pad_name_src[16] = "src";

	sinkpad = gst_element_get_request_pad (streammux, pad_name_sink);
	if (!sinkpad) {
		g_printerr ("Streammux request sink pad failed. Exiting.\n");
		return FALSE;
	}

	srcpad = gst_element_get_static_pad (decode, pad_name_src);

	if (!srcpad) {
		g_printerr ("link: decode request src pad failed. Exiting.\n");
		return FALSE;
	}

	GstPadLinkReturn ret = gst_pad_link (srcpad, sinkpad);
	if (ret != GST_PAD_LINK_OK) {
		g_printerr ("Failed to link decoder to stream muxer. Exiting. %d\n", ret);
		return FALSE;
	}

	gst_object_unref (sinkpad);
	gst_object_unref (srcpad);

	return TRUE;
}

GstElement *skyhub_stream_analysis_bin_new(skyhub_stream_t *stream)
{
	GstElement *bin = NULL;
	gchar bin_name[32] = {0};
	GstElement *queue = NULL;
	GstElement *decode = NULL;
	GstElement *streammux = NULL;
	GstElement *pgie= NULL;
	GstElement *nvvidconv1= NULL;
	GstElement *nvosd= NULL;
	GstElement *nvvidconv2 = NULL;
	GstElement *encode = NULL;
	GstElement *parse = NULL;
	GstElement *mpegtsmux = NULL;
	GstElement *hlssink = NULL;
	GstPad *ghost_pad = NULL;
	GstPad *pad = NULL;
	int index = 0;
	gchar *config_file = NULL;

	g_snprintf (bin_name, 31, "analysis-%d", index);
	/* Create a source GstBin to abstract this bin's content from the rest of the
	 * pipeline */
	bin = gst_bin_new (bin_name);


	queue = create_element("queue", "analysis-queue");

	decode = create_element ("nvv4l2decoder", "decode");
	g_object_set (G_OBJECT(decode), "bufapi-version", TRUE, NULL);	

	streammux = create_element ("nvstreammux", "streammux");

	pgie = create_element ("nvinfer", "primary-nvinference-engine");

	/* Use convertor to convert from NV12 to RGBA as required by nvosd */
	nvvidconv1 = create_element ("nvvideoconvert", "nvvideo-converter");

	nvosd = create_element ("nvdsosd", "nv-onscreendisplay");

	nvvidconv2 = create_element ("nvvideoconvert", "nvvideo-converter2");

	/* Create OSD to draw on the converted RGBA buffer */
	encode = create_element("nvv4l2h265enc", "encode");

	mpegtsmux = create_element("mpegtsmux", "tsmux");

	parse = create_element("h265parse", "h265parse");

	hlssink = create_element("hlssink", "hlssink");

	gst_bin_add_many(GST_BIN(bin),
			queue, 
			decode, 
			streammux, 
			pgie, 
			nvvidconv1, 
			nvosd, 
			nvvidconv2, 
			encode, 
			mpegtsmux, 
			parse,
			hlssink, 
			NULL);


	gst_element_sync_state_with_parent(queue);

	if (!gst_element_link_many(queue, decode, NULL)) {
		printf("Link failed\n");
		exit(-1);
	}

	if (!link_streamux_to_decode(streammux, decode)) {
		printf("Link to streammux failed\n");
		exit(-1);
	}

	if (!gst_element_link_many(streammux, pgie, nvvidconv1, nvosd, nvvidconv2, encode, parse, mpegtsmux, hlssink, NULL)) {
		g_printerr ("Elements could not be linked. Exiting.\n");
		exit(-1);
	}

	pad = gst_element_get_static_pad (queue, "sink");

	g_assert(pad != NULL);

	ghost_pad = gst_ghost_pad_new ("sink", pad);

	g_assert(ghost_pad != NULL);

	
	if (!gst_pad_set_active (ghost_pad, TRUE)) {
		printf("set_active failed\n");
	}

	if (!gst_element_add_pad (bin, ghost_pad)) {
		printf("add_pad failed\n");

	}
	gst_object_unref (pad);

	g_object_set (G_OBJECT (streammux), 
			"width", 4000, 
			"height", 3000, 
			"batch-size", 1,
			"batched-push-timeout", MUXER_BATCH_TIMEOUT_USEC, 
			NULL);

	config_file = skyhub_stream_get_model_config(stream);

	g_assert(config_file != NULL);

	g_object_set (G_OBJECT (pgie),
			"config-file-path", config_file, 
			NULL);

	g_object_set (G_OBJECT (pgie), "batch-size", 1, NULL);

	GstPad *tiler_src_pad = gst_element_get_static_pad (pgie, "src");

	if (!tiler_src_pad)
		g_print ("Unable to get src pad\n");
	else
		gst_pad_add_probe (tiler_src_pad, GST_PAD_PROBE_TYPE_BUFFER,
				tiler_src_pad_buffer_probe, NULL, NULL);
	gst_element_set_state (bin, GST_STATE_PLAYING);

	return bin;
}
